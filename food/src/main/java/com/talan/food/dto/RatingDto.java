package com.talan.food.dto;


import com.talan.food.entities.Product;
import com.talan.food.entities.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RatingDto {

	private Long id;
	private double value;
	private User user;
	private Product product;

	public RatingDto(double value, User user, Product product) {
		super();
		this.setValue(value);
		this.user = user;
		this.product = product;
	}

}
